#!/usr/bin/env python
#coding:utf-8

import numpy as np
import cv2

MIN_CONTOUR_AREA = 100
RESIZED_IMAGE_WIDTH = 20
RESIZED_IMAGE_HEIGHT = 30

class DataContour:
    """
    """

    np_contour = None
    bounding_rect = None
    rect_x = 0
    rect_y = 0
    rect_width = 0
    rect_height = 0
    area = 0.0

    def __init__(bounding_rect):
        """
        """
        self.bounding_rect = bounding_rect
        [x, y, h, w] = self.bounding_rect
        self.rect_x = x
        self.rect_y = y
        self.rect_width = w
        self.rect_height = h


    def contour_valid(self):
        """
        Simple validation check if the contour found is at least a specified size
        """
        if self.area < MIN_CONTOUR_AREA:
            return False
        return True


def train_knn():
    """
    Loads the classifications and the flattened images and trains the KNN

    :return: The knn object
    """
    try:
        npa_classifications = np.loadtxt('out/classifications.txt', np.float32)
        npa_flattened_images = np.loadtxt('out/flattened_images.txt', np.float32)
    except:
        print('Unable to load classifications/flattened_images')
        return False

    npa_classifications = npa_classifications.reshape((npa_classifications.size, 1))
    k_nearest = cv2.ml.KNearest_create()
    k_nearest.train(npa_flattened_images, cv2.ml.ROW_SAMPLE, npa_classifications)

    return k_nearest


def main():
    """
    """
    data_contours = []
    valid_data_contours = []
    knn = train_knn()

    # Load test image and prepare gray, blurred, threshold and threshold copy of the image
    image = cv2.imread('test/1.png')
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image_blurred = cv2.GaussianBlur(image_gray, (5,5), 0)
    image_thresh = cv2.adaptiveThreshold(image_blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2)
    image_thresh_copy = image_thresh.copy()

    # Find contours
    contours, np_contours, np_hierarchy = cv2.findContours(image_thresh_copy, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Setup data_contours list
    for np_contour in np_contours:
        data_contour = DataContour(cv2.boundingRect(np_contour))
        data_contour.np_contour = np_contour
        data_contour.area = cv2.contourArea(np_contour)
        data_contours.append(data_contour)

    # Get valid_data_contours list
    for contour in data_contours:
        if contour.contour_valid():
            valid_data_contours.append(contour)

    # Sort from left => right
    valid_data_contours.sort(key=operator.attrgetter('rect_x'))

    # --- TEST ---
    extracted_string = ""

    for contour in valid_data_contours:
        cv2.rectangle(image,
                      (contour.rect_x, contour.rect_y),
                      (contour.rect_x + contour.rect_width, contour.rect_y + contour.rect_height),
                      (0, 255, 0),
                      2)

        image_roi = image_thresh[contour.rect_y : contour.rect_y + contour.rect_height,
                                 contour.rect_x : contour.rect_x + contour.rect_width]
        image_roi_resized = cv2.resize(image_roi, (RESIZED_IMAGE_HEIGHT, RESIZED_IMAGE_HEIGHT))

        np_roi_resized = image_roi_resized.reshape((1, RESIZED_IMAGE_WIDTH * RESIZED_IMAGE_HEIGHT))
        np_roi_resized = np.float32(np_roi_resized)

        # KNN
        retval, np_results, neigh_resp, dists = knn.findNearest(np_roi_resized, k = 1)

        char = str(chr(int(np_results[0][0])))
        extracted_string += char

    print('Extracted string: ', extracted_string)
    cv2.imwrite('test/out.png', image)

    return

if __name__ == '__main__':
    main()
