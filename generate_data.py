#!/usr/bin/env python
#coding:utf-8

import cv2
import numpy as np
import os
import sys

def _load_image(image):
    """
    Load the specified image by the absolute path

    :param image: absolute path to the image
    :return: cv2 image or false on error
    """
    if not os.path.isfile(image):
        return False

    image = cv2.imread(image)

    if image is None:
        return False
    return image


def main(training_chars_image):
    """
    """
    training_image = _load_image(os.path.join(os.path.dirname(__file__), training_chars_image))

    # Grayscale
    image_gray = cv2.cvtColor(training_image, cv2.COLOR_BGR2GRAY)

    # Blurred
    image_blurred = cv2.GaussianBlur(image_gray, (5,5), 0)

    # Threshold image
    image_threshold = cv2.adaptiveThreshold(image_blurred, 
                                            255,  # Full white for all pixels which pass the threshold
                                            cv2.ADAPTIVE_THRESH_GAUSSIAN_C,  # Gaussian method
                                            cv2.THRESH_BINARY_INV,  # Inverse binary (FG=white)
                                            11, # ROI while calculating the threshold of a pixel
                                            2) # Constant subtracted from the mean

    # Find contours
    image_threshold_copy = image_threshold.copy()
    image_contours, npa_contours, npa_hierarchy = cv2.findContours(image_threshold_copy, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Preparation
    npa_flattened_images = np.empty((0, 20, 30))
    classifications= list()

    # All valid characters we are interested in
    valid_chars = [ord('0'), ord('1'), ord('2'), ord('3'), ord('4'), ord('5'), ord('6'), ord('7'), ord('8'), ord('9'),
                   ord('a'), ord('b'), ord('c'), ord('d'), ord('e'), ord('f'), ord('g'), ord('h'), ord('i'), ord('j'),
                   ord('k'), ord('l'), ord('m'), ord('n'), ord('o'), ord('p'), ord('q'), ord('r'), ord('s'), ord('t'),
                   ord('u'), ord('v'), ord('w'), ord('x'), ord('y'), ord('z')]

    for npa_contour in npa_contours:
        if cv2.contourArea(npa_contour) > 100:
            [x, y, w, h] = cv2.boundingRect(npa_contour)
            cv2.rectangle(training_image, (x, y), (x + w, y + h), (0,0,255), 2)
            image_roi = image_threshold[y:y+h, x:x+w]
            image_roi_resized = cv2.resize(image_roi, (20, 30))

            cv2.imshow('image_roi', image_roi)
            cv2.imshow('image_roi_resized', image_roi_resized)
            cv2.imshow('training_numbers', training_image)

            # Learning
            input = cv2.waitKey(0)
            print('KEY: ', input)

            if input == 27: # ESC for exit
                sys.exit()
            elif input in valid_chars:
                classifications.append(input)
                npa_flattened_image = image_roi_resized.reshape((1, 20 * 30))
                npa_flattened_images = np.append(npa_flattened_images, npa_flattened_image)
            else:
                print('Key is not a valid char')

    float_classifications = np.array(classifications, np.float32)
    npa_classifications = float_classifications.reshape((float_classifications.size, 1))

    print('TRAINING COMPLETE!')

    np.savetxt('out/classifications.txt', npa_classifications)
    np.savetxt('out/flattened_images.txt', npa_flattened_images)

    cv2.destroyAllWindows()
    return

if __name__ == '__main__':
    main('training_chars.png')
